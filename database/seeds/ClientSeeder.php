<?php

use Illuminate\Database\Seeder;

/**
 * Class ClientSeeder
 */
class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Client::class, 5)->create();

        factory(\App\Client::class)->create(['first_name' => 'Test Name',
            'last_name' => 'Test Sirname',
            'email' => 'Test@enta.net']);

    }
}
