<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Superbalist\PubSub\PubSubAdapterInterface;
use Superbalist\PubSub\Adapters\LocalPubSubAdapter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Superbalist\PubSub\PubSubAdapterInterface', 'Superbalist\PubSub\Adapters\LocalPubSubAdapter');
    }
}
