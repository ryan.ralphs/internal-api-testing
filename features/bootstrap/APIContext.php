<?php


require_once __DIR__ . '/../../vendor/laravel/framework/src/Illuminate/Support/Facades/DB.php';

use Behat\Behat\Context\Context;
use Entanet\Behat\APIContext as entaContext;


/**
 * Defines application features from the specific context.
 */
class APIContext extends entaContext implements Context
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @BeforeScenario
     */
    public function setUp()
    {

    }


    /**
     * @AfterScenario
     */
    public function tearDown()
    {

    }


    /**
     * @Then I assert the data returned is an instance of :arg1
     */
    public function iAssertTheDataReturnedIsAnInstanceOf($arg1)
    {
        $this->assertInstanceOf("App\\$arg1", $this->request->original);
    }

    /**
     * @Given I expect the post attempt failed with error code :code
     */
    public function iExpectThePostAttemptFailedWithErrorCode($code)
    {

        $this->assertEquals($code, $this->request->exception->errorInfo[0]);
    }

    /**
     * @Then I expect the post attempt failed with error message :arg1
     */
    public function iExpectThePostAttemptFailedWithErrorMessage($message)
    {
        $this->assertEquals($message, $this->request->exception->errorInfo[2]);
    }

    /**
     * @Then I assert no exceptions were thrown and the request was successful
     */
    public function iAssertNoExceptionsWereThrownAndTheRequestWasSuccessful()
    {
        $this->assertNull($this->request->exception);
    }



}